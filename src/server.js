const express = require('express'); 
const app = express();


const hostname = "localhost";
const port = 8000;

app.get('/hi', (req, res) => {
    res.send("Hello world");
});

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});